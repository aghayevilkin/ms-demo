package com.example.productms.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductResponseDto {

    private Long id;
    private String name;
    private String description;
    private Long price;
}
