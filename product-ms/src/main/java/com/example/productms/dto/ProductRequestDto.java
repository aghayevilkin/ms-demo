package com.example.productms.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductRequestDto {

    private String name;
    private String description;
    private Long price;
    private Integer quantity;
    private String category;
    private boolean isActive;
}
