package com.example.productms.service.impl;

import com.example.productms.domain.Product;
import com.example.productms.dto.ProductRequestDto;
import com.example.productms.dto.ProductResponseDto;
import com.example.productms.repository.ProductRepository;
import com.example.productms.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;
    private final ModelMapper mapper;

    @Override
    public void createProduct(ProductRequestDto productRequestDto) {
        Product product = mapper.map(productRequestDto, Product.class);
        product.setCreatedAt(LocalDateTime.now());
        repository.save(product);
        log.info("created product: {}", product);
    }

    @Override
    public ProductResponseDto findById(Long id) {
        Product product = repository.findById(id).orElseThrow(() -> new RuntimeException("Not Found"));
        log.info("product was found: {}", product);
        return mapper.map(product, ProductResponseDto.class);
    }
}
