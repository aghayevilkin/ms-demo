package com.example.productms.service;

import com.example.productms.dto.ProductRequestDto;
import com.example.productms.dto.ProductResponseDto;

public interface ProductService {
    void createProduct(ProductRequestDto productRequestDto);

    ProductResponseDto findById(Long id);
}
