package com.example.productms.controller;

import com.example.productms.dto.ProductRequestDto;
import com.example.productms.dto.ProductResponseDto;
import com.example.productms.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public void createProduct(@RequestBody ProductRequestDto productRequestDto) {
        productService.createProduct(productRequestDto);
        log.info("request sent to service: {}", productRequestDto);
    }
    @GetMapping("/{id}")
    public ProductResponseDto getProductById(@PathVariable  Long id){
        return productService.findById(id);
    }

}
